<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopifyShopApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopify_shop_application', function (Blueprint $table) {
            $table->increments('shop_application_id');
            $table->unsignedInteger('shop_id')->comment('Shopify Shop');
            $table->unsignedInteger('application_id')->comment('Application');
            $table->smallInteger('status')->default(0)->comment('Status');
            $table->integer('charge_id')->nullable()->comment('Charge Id');
            $table->float('custom_price')->nullable()->comment('Custom Price');
            $table->timestamps();
            $table->foreign('shop_id')->references('shop_id')->on('shopify_shop');
            $table->foreign('application_id')->references('application_id')->on('shopify_application');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopify_shop_application');
    }
}
