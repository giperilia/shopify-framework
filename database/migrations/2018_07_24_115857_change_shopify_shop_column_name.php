<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeShopifyShopColumnName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shopify_configuration', function (Blueprint $table) {
            $table->dropColumn('shopify_app_id');
            $table->unsignedInteger('shop_id');
            $table->foreign('shop_id')->references('shop_id')->on('shopify_shop')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shopify_configuration', function(Blueprint $table)
        {
            $table->dropColumn('shop_id');
            $table->integer('shopify_app_id');
        });
    }
}

