<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShopifyConfigurationAppId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shopify_configuration', function(Blueprint $table)
        {
            $table->unsignedInteger('app_id');
            $table->index('app_id');
            $table->foreign('app_id')->references('application_id')->on('shopify_application')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shopify_configuration', function(Blueprint $table)
        {
            $table->dropIndex('shopify_shop_application_id_index');
            $table->dropForeign('shopify_shop_application_id_foreign');
            $table->dropColumn('app_id');
        });
    }
}
