<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopifyConfigurationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopify_configuration', function (Blueprint $table) {
            $table->increments('configuration_id');
            $table->integer('shopify_app_id');
            $table->string('name')->comment('Configuration Name');
            $table->string('value')->nullable()->comment('Configuration Value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopify_configuration');
    }
}

