<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopifyApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopify_application', function (Blueprint $table) {
            $table->increments('application_id');
            $table->string('name')->unique()->comment('Application Name');
            $table->string('api_key')->nullable()->comment('Shopify Api Key');
            $table->string('api_secret')->nullable()->comment('Shopify Api Secret');
            $table->integer('trial_days')->nullable()->comment('Trial days');
            $table->float('price')->comment('Application Price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopify_application');
    }
}
