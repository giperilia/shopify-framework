<?php

namespace Anvanto\Shopify\Form;

use Encore\Admin\Form\Field;

/**
 * Class CKEditor
 * @package Anvanto\Shopify\Form
 */
class CKEditor extends Field
{
    /**
     * @var array
     */
    public static $js = [
        '/vendor/anvanto/shopify/js/ckeditor/ckeditor.js'
    ];

    /**
     * @var string
     */
    protected $view = 'shopify::ckeditor';

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function render()
    {
        $this->script = "
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '" . csrf_token() . "'
            }
        });
        
        CKEDITOR.replace( $('textarea.{$this->getElementClassString()}').attr('id'), {removePlugins : 'elementspath,save,font,flash',extraPlugins: 'simage,embedbase,embed,autoembed,autolink'  } );
        CKEDITOR.config.embed_provider = '//ckeditor.iframe.ly/api/oembed?url={url}&callback={callback}&api_key=273104d9be0044b0c7a94c';
        CKEDITOR.config.imageUploadURL = '".route('admin.file.save')."';
        CKEDITOR.config.dataParser = function(data){
            if (data.error){
                alert(data.error)
                return false
            }
            return data.url;
        };
        ";
        return parent::render();
    }
}
