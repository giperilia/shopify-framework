<?php
namespace Anvanto\Shopify\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Encore\Admin\Auth\Database\Administrator;

class Shop extends Model
{
    const TABLE_NAME = 'shopify_shop';

    const SHOP_ID = 'shop_id';
    const NAME = 'name';
    const CURRENCY = 'currency';
    const EMAIL = 'email';

    const DEFAULT_CURRENCY = 'USD';

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @var string
     */
    protected $primaryKey = self::SHOP_ID;

    /**
     * @var array
     */
    protected $fillable = [
        self::NAME
    ];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->{self::SHOP_ID};
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->{self::NAME};
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->{self::CURRENCY};
    }

    /**
     * Initialize ShopApplication relation
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function applications()
    {
        return $this->hasMany(
            ShopApplication::class,
            Shop::SHOP_ID,
            Shop::SHOP_ID
        );
    }

    /**
     * @return Application[]
     */
    public function getApplications()
    {
        return $this->applications()->getResults();
    }

    /**
     * @param Administrator $user
     * @throws \Exception
     * @return Shop|int
     */
    public static function getByUser(Administrator $user = null)
    {
        if (null === $user) {
            $user = Auth::guard('admin')->user();
        }

        //Admin user
        if ($user->getAuthIdentifier() == 1) {
            return -1;
        }

        //Shop user
        if ($shop = Shop::where(self::NAME, $user->username)->first()) {
            return $shop;
        }

        throw new \Exception('Please reinstall the application');
    }

    /**
     * @param Administrator $user
     * @throws \Exception
     * @return int
     */
    public static function getIdByUser(Administrator $user = null)
    {
        $shop = self::getByUser($user);
        if ($shop instanceof Shop) {
            return $shop->getId();
        }
        return $shop;
    }
}
