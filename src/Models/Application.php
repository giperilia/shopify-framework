<?php
namespace Anvanto\Shopify\Models;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    const TABLE_NAME = 'shopify_application';

    const APPLICATION_ID = 'application_id';
    const NAME = 'name';
    const API_KEY = 'api_key';
    const API_SECRET = 'api_secret';
    const TRIAL_DAYS = 'trial_days';
    const PRICE = 'price';

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @var string
     */
    protected $primaryKey = self::APPLICATION_ID;

    /**
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::PRICE,
    ];

    /**
     * @var array
     */
    protected $attributes = [
        self::PRICE => 9.99,
        self::TRIAL_DAYS => 14,
    ];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->{self::APPLICATION_ID};
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->{self::NAME};
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->{self::API_KEY};
    }

    /**
     * @return string
     */
    public function getApiSecret()
    {
        return $this->{self::API_SECRET};
    }

    /**
     * @return int
     */
    public function getTrialDays()
    {
        return $this->{self::TRIAL_DAYS};
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->{self::PRICE};
    }

    /**
     * Initialize ShopApplication relation
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function shopApplications()
    {
        return $this->hasMany(
            ShopApplication::class,
            Application::APPLICATION_ID,
            Application::APPLICATION_ID
        );
    }

    /**
     * @return ShopApplication[]
     */
    public function getShopApplications()
    {
        return $this->shopApplications()->getResults();
    }

    /**
     * @param string $name
     * @return Application
     * @throws \Exception
     */
    public static function getByName($name)
    {
        if ($app = Application::where(self::NAME, $name)->first()) {
            return $app;
        }
        throw new \Exception('No applications found');
    }
}
