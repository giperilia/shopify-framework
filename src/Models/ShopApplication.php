<?php
namespace Anvanto\Shopify\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Encryption\Encrypter;

class ShopApplication extends Model
{
    const TABLE_NAME = 'shopify_shop_application';

    const SHOP_APPLICATION_ID = 'shop_application_id';
    const STATUS = 'status';
    const CHARGE_ID = 'charge_id';
    const CUSTOM_PRICE = 'custom_price';
    const TOKEN = 'token';
    const VERSION = 'version';
    const INSTALLED = 'installed';
    const SECRET = '^__^anvanto2^__^';

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @var string
     */
    protected $primaryKey = self::SHOP_APPLICATION_ID;

    /**
     * @var array
     */
    protected $fillable = [
        Shop::SHOP_ID,
        Application::APPLICATION_ID,
        self::CUSTOM_PRICE,
        self::TOKEN
    ];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->{self::SHOP_APPLICATION_ID};
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->{self::STATUS};
    }

    /**
     * @return string
     */
    public function getToken()
    {
        $crypt = new Encrypter(self::SECRET);
        return $crypt->decryptString($this->{self::TOKEN});
    }

    /**
     * @return string
     */
    public function setToken($token)
    {
        $crypt = new Encrypter(self::SECRET);
        $this->{self::TOKEN} = $crypt->encryptString($token);
    }

    /**
     * @return int
     */
    public function getChargeId()
    {
        return $this->{self::CHARGE_ID};
    }

    /**
     * @return float
     */
    public function getCustomPrice()
    {
        return $this->{self::CUSTOM_PRICE};
    }

    /**
     * Initialize shop relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function shop()
    {
        return $this->hasOne(
            Shop::class,
            Shop::SHOP_ID,
            Shop::SHOP_ID
        );
    }

    /**
     * @return Shop
     */
    public function getShop()
    {
        return $this->shop()->getResults();
    }

    /**
     * Initialize application relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function application()
    {
        return $this->hasOne(
            Application::class,
            Application::APPLICATION_ID,
            Application::APPLICATION_ID
        );
    }

    /**
     * @return Application
     */
    public function getApplication()
    {
        return $this->application()->getResults();
    }

    /**
     * @param string $appName
     * @param int $shopId
     * @return ShopApplication
     * @throws \Exception
     */
    public static function find($appName, $shopId = null)
    {
        if (null === $shopId) {
            $user = Auth::guard('admin')->user();
            $shopId = Shop::getIdByUser($user);
        }

        if (empty($shopId) || -1 === $shopId) {
            return null;
        }

        $shopApplication = ShopApplication::where(Shop::SHOP_ID, $shopId)
            ->where(Application::APPLICATION_ID, Application::getByName($appName)->getId())
            ->first();

        if (!$shopApplication) {
            throw new \Exception('The application is not installed for the shop.');
        }

        return $shopApplication;
    }
}
