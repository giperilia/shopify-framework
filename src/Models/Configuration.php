<?php
namespace Anvanto\Shopify\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Anvanto\Shopify\Models\Shop;

/**
 * Class Configuration
 * @package Anvanto\Shopify\Models
 */
class Configuration extends Model
{

    /**
     *@const string
     */
    const CONFIGURATION_ID = 'configuration_id';

    /**
     *@const string
     */
    const SHOP_ID = 'shop_id';

    /**
     *@const string
     */
    const APP_ID = 'app_id';

    /**
     *@const string
     */
    const NAME = 'name';

    /**
     *@const string
     */
    const VALUE = 'value';

    /**
     * @var string
     */
    protected $table = 'shopify_configuration';

    /**
     * @var string
     */
    protected $primaryKey = self::CONFIGURATION_ID;

    /**
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::VALUE,
        self::SHOP_ID,
        self::APP_ID
    ];

    /**
     * Configuration constructor.
     * @param null $shop_id
     * @param null $app_id
     * @param array $attributes
     */
    public function __construct($shop_id = null, $app_id = null, array $attributes = [])
    {
        parent::__construct($attributes);
        if($shop_id){
            $this->{self::SHOP_ID} = $shop_id;
        }
        if($app_id){
            $this->{self::APP_ID} = $app_id;
        }
    }

    /**
     * @param $name
     * @return bool
     */
    public function get($name)
    {
        $item = $this->where(self::NAME,$name)->where(self::SHOP_ID,$this->{self::SHOP_ID})->where(self::APP_ID,$this->{self::APP_ID})->first();
        if ($item){
            return $item->{self::VALUE};
        }
        return false;
    }

    /**
     * @return array|bool
     */
    public function getAll()
    {
        $items = $this->where(self::SHOP_ID,$this->{self::SHOP_ID})->where(self::APP_ID,$this->{self::APP_ID})->get()->toArray();
        if ($items){
            return array_combine(array_column($items, 'name'), array_column($items, 'value'));
        }
        return false;
    }


    /**
     * @param $name
     * @param null $value
     * @return bool
     */
    public function setValue($name, $value = null)
    {
        $item = $this->where(self::NAME,$name)->where(self::SHOP_ID,$this->{self::SHOP_ID})->where(self::APP_ID,$this->{self::APP_ID})->first();
        if (!$item){
            $item = new self($this->{self::SHOP_ID}, $this->{self::APP_ID});
        }
        $item->{self::NAME} = $name;
        $item->{self::VALUE} = $value;
        return (bool)$item->save();
    }

    /**
     * @param $filePath
     * @return bool
     */
    public function installSampleData($filePath)
    {
        if (file_exists($filePath)) {
            $configuration = include $filePath;

            foreach ($configuration as $name => $value){
                $this->setValue($name, $value);
            }
            return true;
        }
        return false;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shop(){
        return $this->belongsTo(Shop::class, self::SHOP_ID, Shop::SHOP_ID);
    }


}
