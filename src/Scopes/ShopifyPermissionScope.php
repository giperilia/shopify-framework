<?php

namespace Anvanto\Shopify\Scopes;

use Anvanto\Shopify\Models\Shop;
use Anvanto\Shopify\Models\ShopApplication;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class ShopifyPermissionScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $shopId = Shop::getIdByUser();
        if ($shopId > 0) {
            $builder->whereIn(ShopApplication::SHOP_APPLICATION_ID, function ($query) use ($shopId) {
                $query->select(ShopApplication::SHOP_APPLICATION_ID)
                    ->from(ShopApplication::TABLE_NAME)
                    ->where(Shop::SHOP_ID, $shopId);
            });
        }
    }
}
