<?php

use Illuminate\Routing\Router;

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => 'Anvanto\Shopify\Controllers\Admin',
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->resource('shopify/shop', 'ShopController');
    $router->resource('shopify/application', 'ApplicationController');
    $router->resource('shopify/shop_application', 'ShopApplicationController');

});

Route::group([
    'namespace'     => 'Anvanto\Shopify\Controllers',
    'middleware'    => ['web'],
], function (Router $router) {

    $router->post('shopify/apps/file_save', 'FileController@save')->name('admin.file.save');
    $router->get('shopify/apps/charge/activate/{id}', 'ChargeController@activate');
    $router->get('shopify/apps/charge/create/{id}', 'ChargeController@create');

});
