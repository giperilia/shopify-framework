<?php
namespace Anvanto\Shopify\Client;

class ShopifyApiException extends \Exception
{
    /**
     * @var string
     */
    protected $method;

    /**
     * @var int
     */
    protected $path;

    /**
     * @var \Throwable
     */
    protected $params;

    /**
     * @var string
     */
    protected $response_headers;

    /**
     * @var string
     */
    protected $response;

    /**
     * ShopifyApiException constructor.
     * @param string $method
     * @param int $path
     * @param \Throwable $params
     * @param mixed $response_headers
     * @param string $response
     */
    public function __construct($method, $path, $params, $response_headers, $response)
    {
        $this->method = $method;
        $this->path = $path;
        $this->params = $params;
        $this->response_headers = $response_headers;
        $this->response = $response;

        parent::__construct(
            $response_headers['http_status_message'],
            $response_headers['http_status_code']
        );
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return int
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return \Throwable
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @return mixed
     */
    public function getResponseHeaders()
    {
        return $this->response_headers;
    }

    /**
     * @return string
     */
    public function getResponse()
    {
        return $this->response;
    }
}
