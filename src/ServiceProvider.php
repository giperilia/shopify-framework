<?php

namespace Anvanto\Shopify;

use Illuminate\Support\ServiceProvider as StandardServiceProvider;
use Encore\Admin\Form;
use Anvanto\Shopify\Form\CKEditor;

class ServiceProvider extends StandardServiceProvider
{
    /**
     * @var array
     */
    protected $commands = [
        'Anvanto\Shopify\Console\InstallCommand',
    ];

    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/shopify.php' => config_path('shopify.php'),
            ]);
        }
        $this->publishes([
            __DIR__.'/../pub' => public_path('vendor/anvanto/shopify'),
        ], 'public');

        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'shopify');

        if (file_exists($routes = __DIR__ . '/../src/routes.php')) {
            $this->loadRoutesFrom($routes);
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->commands($this->commands);
        Form::extend('ckeditor', CKEditor::class);
    }
}
