<?php

/**
 * @param mixed $data
 * @param string $hmacHeader
 * @param string $secret
 * @return bool
 */
function verifyShopifyWebhook($data, $hmacHeader, $secret)
{
    $calculatedHmac = base64_encode(hash_hmac('sha256', $data, $secret, true));
    return ($hmacHeader == $calculatedHmac);
}

/**
 * @param string $url
 * @return bool
 */
function isShopifyUrlValid($url)
{
    $pattern = '/^([a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)+.*)$/';
    return preg_match($pattern, $url);
}
