<?php

namespace Anvanto\Shopify\Console;

use Anvanto\Shopify\Database\AdminConfigurationSeeder;
use Encore\Admin\Auth\Database\Permission;
use Illuminate\Console\Command;

class InstallCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'shopify-framework:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install the shopify-framework package';

    /**
     * Install directory.
     *
     * @var string
     */
    protected $directory = '';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->initDatabase();
    }

    /**
     * Create tables and seed it.
     *
     * @return void
     */
    public function initDatabase()
    {
        $this->call('migrate');

        if (Permission::where('name', 'Shopify Shop')->count() == 0) {
            $this->call('db:seed', ['--class' => AdminConfigurationSeeder::class]);
        }
        $this->call('storage:link');
    }
}
