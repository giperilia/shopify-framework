<?php

namespace Anvanto\Shopify\Middleware;

use Closure;
use Encore\Admin\Auth\Database\Administrator;
use Illuminate\Support\Facades\Auth;

class WebhookAuthenticate
{
    const HEADER_SHOP = 'x-shopify-shop-domain';
    const HEADER_SHA256 = 'x-shopify-hmac-sha256';

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /** @var \App\Http\Controllers\Controller $controller */
        $controller = $request->route()->getController();
        $shop = $request->header(self::HEADER_SHOP);
        $hmac = $request->header(self::HEADER_SHA256);
        $requestJson = file_get_contents('php://input');

        if (!verifyShopifyWebhook($requestJson, $hmac, $controller::SHOPIFY_SECRET)) {
            return response()->json(['error' => 'Auth error']);
        }
        
        $user = Administrator::where('username', $shop)->first();
        if (!$user) {
            return response()->json(['error' => 'Please reinstall the shop']);
        }

        Auth::guard('admin')->login($user, 1);
        return $next($request);
    }
}
