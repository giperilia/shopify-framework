<?php

namespace Anvanto\Shopify\Database;

use Illuminate\Database\Seeder;
use Encore\Admin\Auth\Database\Permission;
use Encore\Admin\Auth\Database\Menu;
use Encore\Admin\Auth\Database\Role;

class AdminConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            [
                'name'        => 'Shopify Shop',
                'slug'        => 'shopify.shop',
                'http_method' => '',
                'http_path'   => '/shopify/shop',
            ],
            [
                'name'        => 'Shopify Application',
                'slug'        => 'shopify.application',
                'http_method' => '',
                'http_path'   => '/shopify/application',
            ],
            [
                'name'        => 'Shopify Shop Application',
                'slug'        => 'shopify.shop.application',
                'http_method' => '',
                'http_path'   => '/shopify/shop_application',
            ],
        ]);

        Menu::insert([
            [
                'parent_id' => 0,
                'order'     => 8,
                'title'     => 'Shopify Management',
                'icon'      => 'fa-shopping-bag',
                'uri'       => '',
            ],
        ]);

        $shopifyMenu = Menu::all()->last();
        Menu::insert([
            [
                'parent_id' => $shopifyMenu->id,
                'order'     => 9,
                'title'     => 'Shops',
                'icon'      => 'fa-shopping-basket',
                'uri'       => 'shopify/shop',
            ],
            [
                'parent_id' => $shopifyMenu->id,
                'order'     => 10,
                'title'     => 'Applications',
                'icon'      => 'fa-cogs',
                'uri'       => 'shopify/application',
            ],
            [
                'parent_id' => $shopifyMenu->id,
                'order'     => 11,
                'title'     => 'Shop Applications',
                'icon'      => 'fa-sitemap',
                'uri'       => 'shopify/shop_application',
            ],
        ]);

        // add role to menu.
        $shopifyMenu->roles()->save(Role::first());
    }
}
