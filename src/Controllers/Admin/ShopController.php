<?php

namespace Anvanto\Shopify\Controllers\Admin;

use Anvanto\Shopify\Models\Shop;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class ShopController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('Shopify Shops');
            $content->description('Installed Shopify Shops');
            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('Edit Shop');
            $content->description('Shop #' . $id);
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('Add a new Shop');
            $content->body($this->form());
        });
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function destroy($id)
    {
        $status = false;
        $message = 'succsess';
        try {
            $status = (bool)Shop::where(Shop::SHOP_ID,$id)->delete();
            if (!$status){
                $message = 'error';
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        return array("status" => $status, 'message' => $message);

    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Shop::class, function (Grid $grid) {
            $grid->shop_id('ID')->sortable();
            $grid->name();
            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Shop::class, function (Form $form) {
            $form->display(Shop::SHOP_ID, 'ID');
            $form->text(Shop::NAME, 'Shop Name');
            $form->text(Shop::CURRENCY, 'Currency');
            $form->text(Shop::EMAIL, 'Email');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
