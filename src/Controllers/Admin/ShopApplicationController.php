<?php

namespace Anvanto\Shopify\Controllers\Admin;

use Anvanto\Shopify\Models\Application;
use Anvanto\Shopify\Models\Shop;
use Anvanto\Shopify\Models\ShopApplication;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class ShopApplicationController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('Shopify Shop Applications');
            $content->description('Shopify Shop Applications');
            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('Shopify Shop Applications');
            $content->description('Shopify Shop Applications');
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('Shopify Shop Applications');
            $content->description('Shopify Shop Applications');
            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(ShopApplication::class, function (Grid $grid) {
            $grid->shop_application_id('ID')->sortable();
            $grid->shop_id('Shop')->display(function($userId) {
                return Shop::find($userId)->name;
            });
            $grid->version('Version')->sortable();

            $grid->application_id('Application')->display(function($applicationId) {
                return Application::find($applicationId)->name;
            });
            $grid->installed('Installed');
            /*$grid->custom_price('Custom Price')->display(function($customPrice) {
                if (null === $customPrice) {
                    return trans('No');
                }
                return '$' . $customPrice;
            });*/

            $grid->status()->display(function($status) {
                if ($status) {
                    return trans('Enabled');
                }
                return trans('Disabled');
            });

            $grid->charge_id();
            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function destroy($id)
    {
        $status = false;
        $message = 'succsess';
        try {
            $status = (bool)ShopApplication::where(ShopApplication::SHOP_APPLICATION_ID, $id)->delete();
            if (!$status){
                $message = 'error';
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        return array("status" => $status, 'message' => $message);

    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(ShopApplication::class, function (Form $form) {
            $form->display(ShopApplication::SHOP_APPLICATION_ID, 'ID');
            $form->text(ShopApplication::VERSION, 'Version');
            $form->select(Shop::SHOP_ID, 'Shop')
                ->options(Shop::all()->pluck(Shop::NAME, Shop::SHOP_ID));

            $form->select(Application::APPLICATION_ID, 'Application')
                ->options(Application::all()->pluck(Application::NAME, Application::APPLICATION_ID));
            $form->select(ShopApplication::INSTALLED, 'Installed')
                ->options(['No','Yes']);
            $form->select(ShopApplication::STATUS, 'Status')
                ->options(['Disabled','Enabled']);
            //$form->currency(ShopApplication::CUSTOM_PRICE, 'Custom Price');
            $form->text('created_at', 'Created At');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
