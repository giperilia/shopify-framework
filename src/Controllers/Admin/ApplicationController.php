<?php

namespace Anvanto\Shopify\Controllers\Admin;

use Anvanto\Shopify\Models\Application;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class ApplicationController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('Applications');
            $content->description('Shopify Applications');
            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('Applications');
            $content->description('Shopify Applications');
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('Create an Applications');
            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Application::class, function (Grid $grid) {

            $grid->application_id('ID')->sortable();
            $grid->name('Name')->sortable();
            $grid->price('Price')->display(function ($price) {
                return '$' . $price;
            })->sortable();

            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Application::class, function (Form $form) {
            $form->display(Application::APPLICATION_ID, 'ID');
            $form->text(Application::NAME, 'Name');
            $form->currency(Application::PRICE, 'Price');
            $form->number(Application::TRIAL_DAYS, 'Trial Days');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
