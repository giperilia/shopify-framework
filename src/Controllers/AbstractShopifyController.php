<?php

namespace Anvanto\Shopify\Controllers;

use Anvanto\Shopify\Client\ShopifyApiException;
use Anvanto\Shopify\Models\Shop;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Anvanto\Shopify\Client\ShopifyClient;
use Anvanto\Shopify\Models\ShopApplication;
use Anvanto\Shopify\Facades\Shopify;
use Encore\Admin\Auth\Database\Administrator;

abstract class AbstractShopifyController extends Controller
{
    const SHOPIFY_API_KEY = '';
    const SHOPIFY_SECRET = '';
    const SHOPIFY_SCOPE = '';

    const APPLICATION_NAME = '';
    const APPLICATION_PATH = '';
    const VERSION = '';
    const INSTALL_REDIRECT_PATH = '/';

    /**
     * @var ShopifyClient
     */
    protected $shopifyClient;

    /**
     * AbstractShopifyController constructor.
     */
    public function __construct()
    {
        $this->shopifyClient = new ShopifyClient(
            static::SHOPIFY_API_KEY,
            static::SHOPIFY_SECRET
        );
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('shopify::login');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function authorizeShop(Request $request)
    {
        Auth::guard('admin')->logout();
        $shop = $request->input('shop');
        $shop = trim(str_ireplace(['http://', 'https://'], '', $shop), '/ ');
        if (!isShopifyUrlValid($shop)) {
            return redirect()->to(static::APPLICATION_PATH)
                ->with('error', 'Shopify URL is not valid!');
        }

        $this->shopifyClient->setShopDomain($shop);


        $pageURL = str_replace('/admin', '', url()->current() . '/install');

        // redirect to authorize url
        header("Location: " . $this->shopifyClient->getAuthorizeUrl(static::SHOPIFY_SCOPE, $pageURL));
        die();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function installShop(Request $request)
    {
        $status = 'success';
        $shop = $request->input('shop');
        $code = $request->input('code');
        $this->shopifyClient->setShopDomain($shop);

        $token = $this->shopifyClient->getAccessToken($code);

        session()->put('shopify_token',$token);
        session()->save();

        if (empty($token)) {
            return redirect()->to(static::APPLICATION_PATH);
        }

        $this->shopifyClient->setToken($token);

        try {
            $shopModel = $this->installPermissions($shop);
            $this->installShopProperties($shopModel);
            $this->installData();
            return redirect()->to($request->root() .'/admin/shopify/apps/' . str_replace( ' ', '_', strtolower(static::APPLICATION_NAME)));
        } catch (ShopifyApiException $e) {
            \Log::error($e);
            return redirect()->to(static::APPLICATION_PATH)
                ->with('error', 'The Shop already installed');
        } catch (\Exception $e) {
            \Log::error($e);
            abort(404);
        }

        return redirect()->to(static::INSTALL_REDIRECT_PATH)
            ->with('status', $status);
    }

    /**
     * @return mixed
     */
    abstract protected function installData();

    /**
     * @param string $shopName
     * @return Shop
     */
    protected function installPermissions($shopName)
    {
        // Create a shop
        $shop = Shopify::installPermissions(
            $shopName,
            static::APPLICATION_NAME,
            static::APPLICATION_PATH,
            static::SHOPIFY_API_KEY,
            static::SHOPIFY_SECRET,
            static::VERSION
        );

        if ($adminUser = $shop->admin_user) {
            session()->put('shopify_shop_id', $shop->getId());
            session()->put('shopify_shop', $shopName);
            session()->save();
            Auth::guard('admin')->login($adminUser, 1);
            unset($shop->admin_user);
        }

        return $shop;
    }

    /**
     * @param Shop $shop
     * @return bool
     * @throws ShopifyApiException
     */
    protected function installShopProperties(Shop $shop)
    {
        $settings = $this->shopifyClient->call(
            'GET',
            '/admin/shop.json'
        );
        return $this->saveShopProperties($shop, $settings ?? []);
    }

    /**
     * @param Shop $shop
     * @param array $data
     * @return bool
     */
    protected function saveShopProperties(Shop $shop, array $data)
    {
        $shop->{Shop::CURRENCY} = $data['currency'] ?? Shop::DEFAULT_CURRENCY;
        $shop->{Shop::EMAIL} = $data['email'] ?? Shop::DEFAULT_CURRENCY;
        return $shop->save();
    }

    /**
     * @param Shop $shop
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function createRecurringApplicationCharge(Shop $shop)
    {
        $shopApp = ShopApplication::find(static::APPLICATION_NAME, $shop->getId());
        $app = $shopApp->getApplication();
        $created = new Carbon($shopApp->created_at);
        $now = Carbon::now();
        $trial_days = $app->getTrialDays() - $created->diff($now)->days;
        if ($trial_days < 0){
            $trial_days = 0;
        }
        $chargeRequest = [
            'recurring_application_charge' => [
                'name' => 'Recurring charge for ' . static::APPLICATION_NAME,
                'price' => $app->getPrice(),
                'return_url' => url()->to('/shopify/apps/charge/activate/' . $shopApp->getId()),
                'trial_days' => $trial_days,
            ]
        ];

        $charge = $this->shopifyClient->call(
            'POST',
            '/admin/recurring_application_charges.json',
            $chargeRequest
        );

        return redirect()->away($charge['confirmation_url']);
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {

        try {
            if ($shop = $request->get('shop')) {
                if (($user = Auth::guard('admin')->user()) && $user->username != $shop) {
                    $user = Administrator::where('username',$shop)->first();
                    if($user){
                        Auth::guard('admin')->setUser($user);
                        session()->invalidate();
                        session()->put('shopify_shop', $shop);
                        session()->save();
                        Auth::guard('admin')->login($user, 1);
                    }
                }

                $shop = Shop::where(Shop::NAME,$shop)->first();
                if($shop){
                    $shopApplication = ShopApplication::find(static::APPLICATION_NAME, $shop->{Shop::SHOP_ID});
                }
                if (Auth::guard('admin')->guest() && $this->shopifyClient->validateSignature($request->all())) {
                    if (!$shop){
                        $this->authorizeShop($request);
                    }
                    if($shop && $shopApplication->installed){
                        $this->installPermissions($shop->name);
                    } else {
                        $this->authorizeShop($request);
                    }
                }
                if (!isset($shopApplication)){
                    $this->authorizeShop($request);
                }
                if (!$shopApplication->installed){
                    $this->authorizeShop($request);
                }
                if (!$shopApplication->status){
                    $created = new Carbon($shopApplication->created_at);
                    $now = Carbon::now();
                    $app = $shopApplication->getApplication();
                    $trial_days = $app->getTrialDays() - $created->diff($now)->days;
                    if ($trial_days < 0){
                        return redirect()->to('/shopify/apps/charge/activate/' . $shopApplication->{ShopApplication::SHOP_APPLICATION_ID});
                    }
                }
                if ($user = Auth::guard('admin')->user()) {
                    session()->put('shopify_shop', true);
                } else {
                    return redirect()->to(static::INSTALL_REDIRECT_PATH);
                }
            }
        } catch (\Exception $e) {
            \Log::error('error: ' . $request->all());
            abort(404, 'Something went wrong, please try again later.');
        }

        return $next($request);
    }
}
