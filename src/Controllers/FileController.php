<?php

namespace Anvanto\Shopify\Controllers;

use Anvanto\Shopify\Models\Shop;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/**
 * Class FileController
 * @package Anvanto\Shopify\Controllers
 */
class FileController extends Controller
{
    /**
     * @const int
     */
    const maxFileSize = 10;

    /**
     * @param Request $request
     * @return string
     * @throws \Exception
     */
    public function save(Request $request)
    {
        $shop = Shop::getIdByUser();
        $file = $request->file('file');
        if ($file){
            if ((($file->getSize() * .0009765625) * .0009765625) > self::maxFileSize){
                return json_encode(array( 'error' => 'Image size must be less than 10mb'));
            }
            if ($shop === -1){
                $image = $file->store('/public/anvanto/shopify');
                return json_encode(array( 'url' => $request->root() . Storage::url($image)));
            } elseif ($shop) {
                $image = $file->storeAs('/public/anvanto/shopify/' . $shop,  $file->hashName());
                return json_encode(array('url' => $request->root() . Storage::url($image)));
            }
        }
        abort(403);
    }
}
