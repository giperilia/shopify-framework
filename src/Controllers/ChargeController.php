<?php

namespace Anvanto\Shopify\Controllers;

use Anvanto\Shopify\Client\ShopifyApiException;
use Anvanto\Shopify\Client\ShopifyClient;
use Anvanto\Shopify\Models\ShopApplication;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ChargeController extends Controller
{
    /**
     * @param Request $request
     * @param int $shopAppId
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function activate(Request $request, $shopAppId)
    {
        $message = 'The subscription is declined. The application is disabled for your shop.';
        /** @var ShopApplication $shopApp */
        $chargeId = $request->get('charge_id');
        $shopApp = ShopApplication::findOrFail($shopAppId);
        $app = $shopApp->getApplication();
        $shop = $shopApp->getShop();
        $shopifyClient = new ShopifyClient(
            $app->getApiKey(),
            $app->getApiSecret(),
            $shop->getName(),
            $shopApp->getToken()
        );
        if(!$chargeId){
            return view('shopify::charge', [
                'error' => true,
                'link' => '/shopify/apps/charge/create/' . $shopApp->{ShopApplication::SHOP_APPLICATION_ID},
                'docs' => $request->root() .'/shopify/apps/' . str_replace( ' ', '_', strtolower($app->getName())) . '/docs'
            ]);
        }
        try {
            if ($shopApp->status){
                $message = sprintf(
                    'You successfully installed the "%s" application!
                            <script>window.location.href="%s";</script>',
                    $app->getName(),
                    $request->root() .'/admin/shopify/apps/' . str_replace( ' ', '_', strtolower($app->getName()))
                );
            } else {
                $chargeInfo = $shopifyClient->call(
                    'GET',
                    '/admin/recurring_application_charges/' . $chargeId . '.json'
                );

                if ($chargeInfo['status'] == 'accepted') {
                    $chargeInfo = $shopifyClient->call(
                        'POST',
                        '/admin/recurring_application_charges/' . $chargeId . '/activate.json',
                        $chargeInfo
                    );
                }

                if ($chargeInfo['status'] == 'active') {
                    $shopApp->charge_id = $chargeInfo['id'];
                    $shopApp->status = 1;
                    $shopApp->save();
                    $message = sprintf(
                        'You successfully installed the "%s" application!
                            <script>setTimeout(function(){window.location.href="%s";}, 2000);</script>',
                        $app->getName(),
                        $request->root() .'/admin/shopify/apps/' . str_replace( ' ', '_', strtolower($app->getName()))
                    );
                    return view('shopify::message', [
                        'message' => $message
                    ]);
                }
            }

        } catch (ShopifyApiException $e) {
            $message = sprintf('Shopify API Error: %s', $e->getMessage());
        }

        return view('shopify::charge', [
            'error' => true,
            'link' => '/shopify/apps/charge/create/' . $shopApp->{ShopApplication::SHOP_APPLICATION_ID},
            'docs' => $request->root() .'/shopify/apps/' . str_replace( ' ', '_', strtolower($app->getName())) . '/docs'
        ]);
    }


    /**
     * @param Request $request
     * @param $shopAppId
     */
    public function create(Request $request, $shopAppId){
        $shopApp = ShopApplication::findOrFail($shopAppId);
        $chargeId = $request->get('charge_id');
        $app = $shopApp->getApplication();
        $shop = $shopApp->getShop();
        $token = session()->get('shopify_token');
        $shopifyClient = new ShopifyClient(
            $app->getApiKey(),
            $app->getApiSecret(),
            $shop->getName(),
            $shopApp->getToken()
        );
        $created = new Carbon($shopApp->created_at);
        $now = Carbon::now();
        $trial_days = $app->getTrialDays() - $created->diff($now)->days;
        if ($trial_days < 0){
            $trial_days = 0;
        }
        $chargeRequest = [
            'recurring_application_charge' => [
                'name' => 'Recurring charge for ' . $app->getName(),
                'price' => $app->getPrice(),
                'return_url' => url()->to('/shopify/apps/charge/activate/' . $shopApp->getId()),
                'trial_days' => $trial_days
            ]
        ];
        $charge = $shopifyClient->call(
            'POST',
            '/admin/recurring_application_charges.json',
            $chargeRequest
        );
        $message = sprintf('<script>top.window.location="%s"</script>', $charge['confirmation_url']);
        return view('shopify::message', [
            'message' => $message
        ]);
    }
}
