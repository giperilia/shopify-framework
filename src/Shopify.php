<?php

namespace Anvanto\Shopify;

use Anvanto\Shopify\Models\Application;
use Anvanto\Shopify\Models\Shop;
use Anvanto\Shopify\Models\ShopApplication;
use Illuminate\Support\Facades\DB;
use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Auth\Database\Role;
use Encore\Admin\Auth\Database\Permission;
use Illuminate\Encryption\Encrypter;

/**
 * Class Shopify.
 */
class Shopify
{
    /**
     * @param string $shopName
     * @param string $applicationName
     * @param string $applicationPath
     * @param string $apiKey
     * @param string $apiSecret
     * @throws \Exception
     * @return Shop
     */
    public static function installPermissions(
        $shopName,
        $applicationName,
        $applicationPath,
        $apiKey,
        $apiSecret,
        $applicationVersion
    ) {
        try {
            DB::beginTransaction();
            // Create a shop
            $shop = Shop::firstOrCreate([
                Shop::NAME => $shopName
            ]);

            // Get the application
            $app = Application::firstOrNew([
                Application::NAME => $applicationName
            ]);

            if (!$app->exists) {
                $app->api_key = $apiKey;
                $app->api_secret = $apiSecret;
                $app->save();
            }

            // Generate relation Shop + Application
            $shopApplication = ShopApplication::firstOrNew([
                Shop::SHOP_ID => $shop->shop_id,
                Application::APPLICATION_ID => $app->application_id
            ]);

            if (!$shopApplication->exists || !$shopApplication->installed) {
                $shopApplication->setToken(session()->get('shopify_token'));
                $shopApplication->{ShopApplication::VERSION} = $applicationVersion;
                $shopApplication->{ShopApplication::INSTALLED} = true;
                $shopApplication->save();
            }

            // Has admin management:
            if (!empty($applicationName)) {
                // Get common role
                $role = Role::firstOrCreate([
                    'name' => 'Shopify',
                    'slug' => 'Shopify'
                ]);

                // Get permission
                $permission = Permission::firstOrCreate([
                    'name' => $applicationName,
                    'slug' => $applicationName,
                    'http_path' => $applicationPath . '*'
                ]);

                // Create a user
                /** @var Administrator $adminUser */
                $adminUser = Administrator::firstOrNew([
                    'username' => $shopName,
                    'name' => $shopName,
                ]);

                if (!$adminUser->exists) {
                    $adminUser->password = bcrypt($shopName);
                    $adminUser->save();

                    // Set role and permission to the user
                    $adminUser->roles()->save($role);
                    $adminUser->permissions()->save($permission);
                }

                $shop->admin_user = $adminUser;
            }

            DB::commit();
            return $shop;
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
