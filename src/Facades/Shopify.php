<?php

namespace Anvanto\Shopify\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Shopify.
 */
class Shopify extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return \Anvanto\Shopify\Shopify::class;
    }
}
